// Service is an interface that exposes the methods of an implementation whose details have been abstracted away.

package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ResponseBody;

public interface PostService {
    // Creating a post
    void createPost(String stringToken, Post post);
    // viewing all post
    Iterable<Post> getPosts();
    // Delete a post
    ResponseEntity deletePost(Long postid,String stringToken);
    // Update a post
    ResponseEntity updatePost(Long id,String stringToken, Post post);
    // To get all post of a specific user
    Iterable<Post> getMyPosts(String stringToken);


}
